<?php

namespace Drupal\oos\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Returns responses for oos routes.
 */
class OosController extends ControllerBase {

  /**
   * Builds the response.
   */
  public function build() {

    $build['content'] = [
      '#type' => 'item',
      '#markup' => $this->t('It works!'),
    ];

    return $build;
  }

}
