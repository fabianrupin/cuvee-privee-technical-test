<?php

namespace Drupal\oos\EventSubscriber;

use Drupal\commerce_cart\CartManagerInterface;
use Drupal\commerce_cart\Event\CartEvents;
use Drupal\commerce_cart\Event\CartOrderItemUpdateEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Cart Event Subscriber.
 */
class OosSubscriber implements EventSubscriberInterface {

  private $cartManager;

  public function __construct(CartManagerInterface $cartManager) {
    $this->cartManager = $cartManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    return [
      CartEvents::CART_ORDER_ITEM_UPDATE => ['updateCart']
    ];
  }

  /**
   * @param \Drupal\commerce_cart\Event\CartOrderItemUpdateEvent $event
   *   The cart update event.
   */
  public function updateCart(CartOrderItemUpdateEvent $event) {
    $cart = $event->getCart();
    $items = $cart->getItems();
    foreach ($items as $item){
      /** @var \Drupal\commerce_product\Entity\Product $product */
      $product = $item->getPurchasedEntity()->getProduct();

      $isOutOfStock = (bool) $product->get('field_out_of_stock')->getValue();
      if($isOutOfStock){
        $this->cartManager->removeOrderItem($cart, $item);
      }
    }
  }

}
